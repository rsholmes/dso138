# DSO138 Oscilloscope

This is the JYE Tech DSO138 Oscilloscope put behind a 10 cm wide Kosmo format panel. 

Slide switches, tactile buttons, and trigger indicator LED are mounted to an auxiliary PCB mounted behind the panel. Also on this board are a Eurorack style 10-pin power header with 10 µF caps and power reversal diodes, and a 9 volt regulator and TL072 op amp with associated passive components.

A BNC jack and a 1/4" phone input jack are mounted on the front panel. A toggle switch selects whether the scope input is connected to the BNC or to the 1/4" jack signal after being buffered with the TL072. The latter signal, buffered, also goes to a second 1/4" panel mounted jack, so the signal can be passed on to another module.

Also on the front panel is another toggle switch, controlling the 9 V power. (It's good to power off the scope when not in use, for at least three reasons: the scope draws about 130 mA current on the +12 V rail, it probably puts out some digital noise you don't want getting into your synth sound, and leaving it powered off when not in use will help extend the life of the display.)

Connections to and from each slide switch are sent to the DSO138 switch footprints via an 8-conductor ribbon cable. A fourth ribbon cable connects the tactile buttons, both sides of the LED, and the input signal to the appropriate pads on the main board. Two more wires carry 9 V and ground to that board, which is mounted perpendicular to the front panel. I used a 3D printed carrier and long spacers to attach it.

The display PCB is mounted to the front panel and is connected to the main PCB via a 40 conductor ribbon cable.

## Current draw
116 mA +12 V, 4 mA -12 V  
(8 mA +12 V, 4 mA -12 V with scope power off)

## Photos

![front](Images/front.jpg)

## Documentation

* [Schematic (auxiliary board)](Docs/dso138.pdf)
* PCB layout: [front](Docs/dso138_layout_front.pdf), [back](Docs/dso138_layout_back.pdf)
* [BOM](Docs/dso138_bom.md)
* [Build notes](Docs/build.md)

## Git repository

* [https://gitlab.com/rsholmes/dso138](https://gitlab.com/rsholmes/dso138)

